# Introducion

## Que es Gamepop ?

GamePop es una plataforma que implementa, escala y administra servicios para juegos.
La plataforma permite a los estudios implementar una suite de mecanicas de disenio,
gestionar los datos del juego y sus jugadores; mantener una economia interna, procesar compras con moneda real.

La plataforma esta disenada para soportar una escala masiva. GamePop tambien puede ser utilizada por estudios grandes en sus propios servers.

Los costos de la plataforma acompanan el crecimiento del juego, la plataforma no tienen ningun costo por adelantado.

Esta plataforma es desarollada por <a href="https://gamepop.tech" target="\_blank">@esimionato</a> con el deseo de democratizar las buenas practicas en la industria de videojuegos, poniendo al alcance de todos una herramienta de disenio y gestion de video juegos tan completa conmo las que utilizan los grandes estudios.

### Ecosistema de la Plataforma
Gamepop esta compuesto por:

  - [Cloud Services]() - componente central, una suite de servicios en la nube pensados para el disenio y gestion de video juegos, Backend-as-a-Service (BaaS).
  La plataforma cuenta con distintos tipos de apis: full rest api, otra implementada con websocket para una comunicacion reactiva, y por ultimo una implementacion clasica con sockets.

  - [Client Libraries]() - librerias clientes para facilitar el uso de los servicios desde el codigo de los juegos (SDKs). C#, C++, Javascript y mas...

  - [Panel de Gestion]() - un panel web en el que podes disenar configurar y gestionar los juegos.

### Integracion con Motores de Juegos
Gamepop esta integrado con los mejores motores:

  + [Unity]() - gamepop incluye una libreria unity desarrollada en c#, junto con juegos de ejemplo y tutoriales.

  + [Unreal]() - gamepop tambien mantiene una libreria en c++, pensada para unreal con ejemplos y tutoriales.

## Primeros Pasos
Un mundo de posibilidades !

Te brindamos lo que necesites para empezar a desarrollar los mejores juegos.
Esta seccion te proporciona algunos conceptos basicos de como funciona la api de gamepop.
Los detalles sobre el uso los podes encontrar en los [api docs](api.md) y en las [guias](guides.md).

La introduccion esta organizada en las siguientes secciones:

  * [Administra tus juegos]() - conoce el panel de gestion de juegos, inicia a trabajar con la plataforma.
  
  * [Authenticacion y perfil de los jugadores]() - como identifica gamepop a los jugadores.
  
  * [Propiedades, estadisticas de los jugadores]() - una breve descripcion de como podes definir y trabajar con datos y estadistas.
  
  * [Gamificacion]() - definicion rapida de las funciones de gamificacion que provee gamepop para tus juegos.
  
  * [Monetizacion]() - se trata de monedas virtuales, productos y precios para el catalog de tus juegos.
