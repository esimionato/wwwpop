<!DOCTYPE html>
<html>
<head>
    <title>GAME-POP | Desarrolla tus juegos mas rapido y mejor</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />

    <meta name="keywords" content="game backend, game developer, multiplayer, tiempo real" />
    <!-- Search Engine -->
    <meta name="description" content="Enfocate en lo divertido de tus juegos, nosotros nos encargamos del resto!">
    <meta itemprop="name" content="GAME-POP | Crea tus juegos mas rapido">
    <meta itemprop="description" content="Enfocate en lo divertido de tus juegos, nosotros nos encargamos del resto!">
    <link rel="shortcut icon" href="assets/images/nube.ico" />
    <link rel="icon" type="image/png" href="assets/images/nube.png" />
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.css" />
    <script type="text/javascript" src="assets/js/modernizr.custom.29473.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!-- jQuery -->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <!-- Owl Clientes Carousel -->
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

    <script>
      $(function() {
        $( "#accordion" ).accordion();
      });
    </script>

    <script>
        function init() {
            window.addEventListener('scroll', function(e){
                var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                    shrinkOn = 200,
                    header = document.querySelector("header");
                if (distanceY > shrinkOn) {
                    classie.add(header,"smaller");
                } else {
                    if (classie.has(header,"smaller")) {
                        classie.remove(header,"smaller");
                    }
                }
            });
        }
        window.onload = init();
    </script>

    <script src="assets/js/classie.js"></script>
</head>

<body>
    <div id="wrapper">
        <header>
    <div class="container clearfix">
        <a href="index.html">
            <h1 id="logo">
              GAME-POP<sup>platform</sup>
              <img src="assets/images/nube.png" style="max-width:40px;">
            </h1>
        </a>
        <nav>
            <a href="ar/pricing.html" class="text-link" id="headerPricingLnk">Precios</a>
            <a href="ar.html#support" class="text-link" id="headerSupportLnk">Soporte</a>
            <a href="/login/" class="text-link" id="headerLoginLnk">Ingresar</a>
            <a href="/singup" class="btn tryout" role="button" id="headerTryLnk">Probar gratis</a>
        </nav>
    </div>
</header>

<section class="hero" name="top">
            <div class="container"> 
                <div class="row">   
                    <div class="col-md-12">
                        <h1 class="text-center first-call">¿ Desarrollas Juegos ?</h1>
                    </div>

                    <div class="col-md-6 slogan">
                        <h2>Armalos Divertidos.</h2>
                        <h2>Armalos mas rapido y mejor.</h2>

                        <div class="divider-white"></div>

                        <div class="intro-text">
                            Si desarrollas Juegos, tenemos la herramienta para vos, para que sepas todo lo que tenés que saber en tiempo real y agilices todo el desarrolo y las operaciones de post-lanzamiento.
                        </div>

                        <div class="call-to-action">
                            <a class="btn btn-default cta" href="/register" role="button">Probar gratis ahora
                            <div class="trial-detail">sin costo y sin tarjeta de crédito</div></a>
                        </div>
                    </div>

                                        <div class="col-md-6 video-container hidden-xs">
                        <div class="video-wrapper hidden-xs">
                            <img src="assets/images/pop_demo_001.gif" width="585" height="329" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></img>
                            <img src="assets/images/pop_demo_002.gif" width="585" height="329" style="position: absolute; left: 90px; top: 120px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></img>
                        </div>
                    </div>
                                    </div>
            </div>
        </section>

        <div id="main" class="clearfix">
            <div id="content">
                <!-- start: clients -->
                <!--section class="free-trial">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="owl-carousel">        
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/fravega.jpg" alt="Fravega">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/garbarino.jpg" alt="Garbarino">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/disney.jpg" alt="Disney">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/avalanchacom.jpg" alt="Avalancha">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/adidas.jpg" alt="Adidas">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/sportline.jpg" alt="Sportline">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/planetabb.jpg" alt="Planeta Bebé">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/x28alarmas.jpg" alt="X-28 Alarmas">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/depot.jpg" alt="Depot">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/necxus.jpg" alt="Necxus">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/maxihogar.jpg" alt="Maxihogar">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/planetazenok.jpg" alt="Planeta Zenok">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/prosmarts.jpg" alt="Prosmarts">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/megafoto.jpg" alt="Megafoto">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/gnmusic.jpg" alt="GN Music">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/kadursport.jpg" alt="Kadursport">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/armytech.jpg" alt="Army Tech">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/babyshopping.jpg" alt="Baby Shopping">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/danitex.jpg" alt="Danitex">
                                  <img class="owl-lazy" data-src="../assets/images/clients/ar/shoesbaires.jpg" alt="ShoesBayres">
                                </div>
                            </div>
                        </div>
                    </div>
                </section-->
                <!-- end: clients -->

                <section class="herramientas-analisis">
                    <div class="analisis">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-md-offset-2 analisis-txt">
                                    <span class="icon chart">
                                    </span>
                                </div>
                                <div class="col-md-7">
                                    <span class="text ">
                                        <h3>Métricas y análisis</h3>
                                        <h4>
                                            Tomá las mejores decisiones para tu juego
                                        </h4>
                                    </span>
                                </div>
                                <div class="col-md-12">
                                    <a href="ar.html#metrics">
                                        <div class="fold-close wow fadeInDown"></div>
                                     </a>                             
                                </div>                            
                            </div>
                        </div>
                    </div>
                </section>
                <span class="anchor" name="metrics"></span>
                <section class="module module-1">
                    <div class="mercado">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1 text-center hidden-xs hidden-sm">
                                    <img src="assets/images/screenshots/ar/mercado.png" style="" class="wow fadeInUp" width="100%">
                                </div>
                                <div class="col-md-5 col-md-offset-1">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon market-icon"></span>
                                        <span class="title-text">
                                            Mercado
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        ¡Conocé el 100% de lo que sucede en tu Juego!
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Navegá todas las categorías y subcategorías de MercadoLibre.</li>
                                            <li>Accedé a rankings de vendedores y publicaciones.</li>
                                            <li>Conocé tendencias, concentraciones de mercado y participación de mercado de cada una.</li>
                                            <li>Descubrí oportunidades y tomá las mejores decisiones para tu negocio.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 col-md-offset-1 text-center visible-xs visible-sm">
                                    <img src="assets/images/screenshots/ar/mercado.png" style="" class="wow fadeInUp" width="100%">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="module module-2">
                    <div class="competencia">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1 text-center hidden-xs hidden-sm">
                                    <img src="assets/images/screenshots/ar/competition.png" style="" class="wow fadeInUp" width="100%">
                                </div>
                                <div class="col-md-5 col-md-offset-1">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon competition-icon"></span>
                                        <span class="title-text">
                                            Competencia
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        Seguí en tiempo real a tus competidores, enterate de todo lo que hacen y venden y reaccioná más rápido!
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Enterate cuando cambian sus precios, crean nuevas publicaciones, modifican stock o títulos, y más.</li>
                                            <li>Conocé al detalle todos sus productos vendidos cada día.</li>
                                            <li>Compará cómo está tu cuenta en relación con las de ellos.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 col-md-offset-1 text-center visible-xs visible-sm">
                                    <img src="assets/images/screenshots/ar/competition.png" style="" class="wow fadeInUp" width="100%">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="module module-3">   
                    <div class="dashboard">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon dashboard-icon"></span>
                                        <span class="title-text">
                                            Panel de indicadores
                                        </span>
                                    </div>
                                    <div class="module-subtitle">
                                        Conocé en un sólo lugar y en tiempo real la información más relevante de tu cuenta
                                    </div>
                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Actividad del día.</li>
                                            <li>Performance de tus publicaciones.</li>
                                            <li>Salud de tu cuenta.</li>
                                            <li>Evolución diaria de ventas, preguntas, visitas y dinero transaccionado.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <img src="assets/images/screenshots/ar/dashboard.png" style="" class="wow fadeInUp" width="100%">
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

                <section class="module module-4" id="asd">   
                    <div class="listings">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-1 hidden-xs hidden-sm">
                                    <img src="assets/images/screenshots/ar/publicaciones_mla.png" style="margin-top:30px;" class="wow fadeInUp img-responsive">
                                </div>
                                <div class="col-md-5">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon listings-icon"></span>
                                        <span class="title-text">
                                            Publicaciones
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        Conocé el puntaje de todas tus publicaciones
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Recibí consejos para mejorar el posicionamiento de todas tus publicaciones y vendé más.</li>
                                            <li>Evaluá la performance individual de cada publicación.</li>
                                            <li>Conocé qué es lo más buscado por los compradores.</li>
                                            <li>Ingresá términos de búsqueda una única vez y conocé en todo momento la posición de tus publicaciones.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-1 visible-xs visible-sm">
                                    <img src="assets/images/screenshots/ar/publicaciones_mla.png" style="margin-top:30px;" class="wow fadeInUp img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="herramientas-gestion">
                    <div class="gestion">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-md-offset-2 gestion-txt">
                                    <span class="icon chart">
                                    </span>
                                </div>
                                <div class="col-md-7">
                                    <span class="text ">
                                        <h3>Herramientas de gestión</h3>
                                        <h4>
                                            Optimizá al máximo la gestión de tus ventas
                                        </h4>
                                    </span>
                                </div>
                                <div class="col-md-12">
                                    <a href="ar.html#management">
                                        <div class="fold-close wow fadeInDown"></div>
                                     </a>                             
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <span class="anchor" name="management"></span>
                <section class="module module-5">   
                    <div class="questions">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon questions-icon"></span>
                                        <span class="title-text">
                                            Preguntas
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        Contestá mucho más rápido las preguntas de tus interesados y concretá más ventas!
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Respuestas rápidas personalizables, para ahorrar tiempo y contestar primero.</li>
                                            <li>Saludo y firma automáticos + atajos de teclado.</li>
                                            <li>Cálculo automático de costo de MercadoEnvíos.</li>
                                            <li>Historial de preguntas, con tiempos de respuesta e información para que sepas cuáles se transformaron en ventas.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <img src="assets/images/screenshots/ar/questions.png" style="" class="wow fadeInUp" width="100%">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="module module-9">   
                    <div class="mailing">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1 hidden-xs hidden-sm">
                                    <img src="assets/images/screenshots/ar/mailing.png" style="" class="wow fadeInUp img-responsive">
                                </div>
                                <div class="col-md-5 col-md-offset-1">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon mailing-icon"></span>
                                        <span class="title-text">
                                            Mensajes automáticos
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        Enviá mensajes a tus compradores y optimizá la gestión de tus ventas
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Enviamos mensajes automáticamente a tus compradores en el momento en que te compraron, se acreditó su pago y más.</li>
                                            <li>A aquellos que ofertaron y no concretaron, les recordamos también de su compra.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6 visible-xs visible-sm">
                                    <img src="assets/images/screenshots/ar/mailing.png" style="" class="wow fadeInUp img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </section> 


                <!-- start: Buyers Module -->
                <section class="module module-8"> 
                    <div class="buyers-module">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon buyers-icon"></span>
                                        <span class="title-text">
                                            Compradores
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        Accedé a toda tu base de compradores!
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Conocé quiénes son tus compradores más importantes.</li>
                                            <li>Accedé a los datos de tus compradores desde tu primera venta.</li>
                                            <li>Filtrá y exportá los que quieras para tus campañas de marketing.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 col-md-offset-1">
                                    <img src="assets/images/screenshots/ar/buyers.png" style="" class="wow fadeInUp img-responsive">
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: Buyers Module -->                        


                <section class="module module-7">   
                    <div class="sales-module">
                        <div class="container">
                            <div class="row">

                                <div class="col-md-5 col-md-offset-1 hidden-xs hidden-sm">
                                    <img src="assets/images/screenshots/ar/sales.png" style="" class="wow fadeInUp img-responsive">
                                </div>

                                <div class="col-md-5 col-md-offset-1">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon sales-icon"></span>
                                        <span class="title-text">
                                            Ventas
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        Gestioná todas tus operaciones de la forma más sencilla
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Administrá el seguimiento de cada venta: cobro, envío, calificación, etc.</li>
                                            <li>Calificá de forma individual o masiva tus ventas</li>
                                            <li>Imprimí tus etiquetas de MercadoEnvíos de la forma más rápida</li>
                                            <li>Accedé a un detalle diario de todas tus ventas, conocé tus productos más vendidos y exportá todo si queres armar tus propios reportes.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6 visible-xs visible-sm">
                                    <img src="assets/images/screenshots/ar/sales.png" style="" class="wow fadeInUp img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>



                <!-- start: publicador -->
                <section class="module module-4 lister">
                    <div class="lister">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon lister-icon"></span>
                                        <span class="title-text">
                                            Publicador
                                        </span>
                                    </div>
                                    <div class="module-subtitle">
                                        Subí y actualizá masivamente todas tus publicaciones
                                    </div>
                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Subí toda la oferta de productos que aún no publicaste</li>
                                            <li>Actualizá masivamente todos tus precios, stock, etc.</li>
                                            <li>Cargá todos los atributos necesarios para posicionarte mejor</li>
                                            <li>Soportá productos con variaciones de talle y color</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 col-md-offset-1 hidden-xs hidden-sm">
                                    <img src="assets/images/screenshots/ar/publicador.png" style="" class="wow fadeInUp img-responsive">
                                </div>
                                <div class="col-md-6 col-md-offset-1 visible-xs visible-sm">
                                    <img src="assets/images/screenshots/ar/publicador.png" style="margin-top:30px;" class="wow fadeInUp img-responsive">
                                </div>                                
                            </div>
                        </div>
                    </div> 
                </section>
                <!-- end: publicador --> 


                <!-- start: descriptions -->
                <section class="module descriptions">
                    <div class="descriptions">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1 hidden-xs hidden-sm">
                                    <img src="assets/images/screenshots/ar/descriptions.png" style="" class="wow fadeInUp img-responsive">
                                </div>
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon descriptions-icon"></span>
                                        <span class="title-text">
                                            Descripciones
                                        </span>
                                    </div>                
                    
                                    <div class="module-subtitle">
                                        Actualizá de forma masiva todas tus descripciones de texto sin formato
                                    </div>
                                    <div class="module-features">
                                        <ul class="items">
                                            <li>En tres simples pasos cargás o actualizás todas juntas</li>
                                            <li>Evitá penalizaciones en tus publicaciones por no tenerlas cargadas</li>
                                            <li>Te sugerimos una descripción de texto sin formato a partir de tu descripción HTML</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-1 visible-xs visible-sm">
                                    <img src="assets/images/screenshots/ar/descriptions.png" style="margin-top:30px;" class="wow fadeInUp img-responsive">
                                </div>                                 
                            </div>
                        </div>
                    </div> 
                </section>
                <!-- end: descriptions -->
                <section class="module module-4">   
                    <div class="operators-blocks">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1 blocks">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon blocking-icon"></span>
                                        <span class="title-text">
                                            Bloqueos
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        Olvidate de las bonificaciones!
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                                
                                            <li>Bloqueá automáticamente las compras duplicadas.</li>
                                            <li>Administrá tus usuarios bloqueados para comprar y preguntar.</li>
                                            <li>No pierdas tiempo, reputación ni dinero.</li>

                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-5 col-md-offset-1 operators">
                                    
                                    <div class="module-title wow fadeInDown">
                                        <span class="icon operators-icon"></span>
                                        <span class="title-text">
                                            Operadores
                                        </span>
                                    </div>

                                    <div class="module-subtitle">
                                        ¿Distintas personas te ayudan a administrar tu cuenta?
                                    </div>

                                    <div class="module-features">
                                        <ul class="items">
                                            <li>Crea usuarios operadores para cada uno de ellos.</li>
                                            <li>Configura a qué puede acceder cada uno.</li>
                                            <li>Conoce sus desempeños y tiempos de respuesta.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <!-- start: multi -->
                <section class="multi-account">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-10">
                                <div class="heading">
                                    Conectá todas tus cuentas
                                </div>
                                <p>Si tenés más de una cuenta de MercadoLibre, te ofrecemos soporte multicuenta para que gestiones todo más rápido desde un mismo lugar.</p>
                            </div>

                            <div class="col-xs-2 multi-img">
                                <img src="assets/images/users-linked.png" style="" class="wow fadeInUp img-responsive">
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: multi -->
                <!-- start: app -->
                <section class="app">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4 text-right">
                                <div class="feature-box">
                                <div class="header">
                                    App para celular
                                </div>                                    
                                    <!-- feature-title -->
                                    <div class="feature-title">
                                        <h4>Estés donde estés</h4>
                                    </div><!-- /feature-title -->

                                    <!-- feature-desc -->
                                    <div class="feature-desc">
                                        <p>En la oficina, en la calle o donde estés, nuestra app te acompaña para que hagas todo en tiempo real y vendas mucho más.</p>
                                    </div><!-- /feature-desc -->
                                </div>

  
                            </div>
                            <div class="col-sm-4 text-center phone-container">
                                <img src="assets/images/phone.png" style="" class="wow fadeInUp img-responsive">
                            </div>
                            <div class="col-sm-4 text-left">
                                <div class="feature-box right">
                                    <!-- feature-title -->
                                    <div class="feature-title">
                                        <h4>A tu medida</h4>
                                    </div><!-- /feature-title -->

                                    <!-- feature-desc -->
                                    <div class="feature-desc">
                                        <p>Podés descargarla tanto en dispositivos Apple (iOS) como en dispositivos Android.</p>
                                    </div><!-- /feature-desc -->
                                </div>  

                                <div class="feature-box second right">
                                    <!-- feature-title -->
                                    <div class="feature-title">
                                        <div class="row">
                                            <div class="col-xs-6"><a href="https://itunes.apple.com/app/id1164234424" target="_blank"><img src="assets/images/app-store-es.png" style="" class="wow fadeInUp img-responsive"></a></div>
                                            <div class="col-xs-6"><a href="https://play.google.com/store/apps/details?id=com.real_trends&hl=es-419" target="_blank"><img src="assets/images/google-play-es.png" style="" class="wow fadeInUp img-responsive"></a></div>
                                        </div>
                                    </div><!-- /feature-title -->
                                </div>       
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: app -->
                <!-- start: password -->
                <section class="password-safe">
                    <div class="pre"></div>
                    <div class="heading">
                        Nunca sabremos tu contraseña
                    </div>
                    <div class="subtext">
                        Nos preocupa la seguridad tanto como a vos.<br />
                        Es por eso que cuando escribís tu usuario y contraseña
                        para ingresar, siempre lo hacés en MercadoLibre.
                    </div>

                    <div class="meli-certified">
                        <a href="https://apps.mercadolibre.com.ar/real-trends" target="_blank" id="bodyCertifiedBtn">
                            <img src="assets/images/seal.png" width="" class="img-responsive">
                        </a>
                    </div>
                    <div class="post"></div>
                </section>
                <!-- end: password -->
                
                <!-- start: statistics -->
                <section class="statistics">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <div class="stat-number wow fadeInUp">
                                    +60 millones
                                </div>
                                <div class="stat-description wow fadeIn">
                                    de preguntas por año<br />respondidas más rápido
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="stat-number wow fadeInUp"> 
                                    +30.000
                                </div>
                                <div class="stat-description wow fadeIn">
                                    competidores analizados<br/>en tiempo real
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="stat-number wow fadeInUp">
                                    +45 millones
                                </div>
                                <div class="stat-description wow fadeIn">
                                   de ventas por año<br />mejor gestionadas
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: statistics -->
                <!-- start: support -->
                <section name="support" class="support text-center">
                    <h2 class="support-title">¿Tenés preguntas?</h2>
                    <h3 class="support-subtitle">¡Estaremos felices de poder responderte!</h3>
                    <div class="container">
                        <div class="row hidden-xs">
                            <div class="col-xs-12 mail">
                                <div class="col-xs-1 col-xs-offset-4"><span class="icon"></span></div>
                                <div class="col-xs-6 text-left"> <div class="value"><a href="mailto:contacto@real-trends.com">contacto@real-trends.com</a></div></div>

                            </div>
                            <div class="col-xs-12 phone">
                                 <div class="col-xs-1 col-xs-offset-4">
                                    <span class="icon"></span>
                                </div>
                                <div class="col-xs-6 text-left">
                                    <div class="value">
                                        (011) 5368-2241
                                        <div class="second-line">
                                            lunes a viernes de 10 a 13 y de 15 a 18h
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row visible-xs">
                            <div class="text-center mail-mobile">
                                <span class="mail-mobile"><a href="mailto:contacto@real-trends.com">contacto@real-trends.com</a></span>
                            </div>
                            <div class="text-center phone-mobile">
                                <span class="phone-mobile">(011) 5368-2241</span>
                                <span class="phone-details-mobile">lunes a viernes de 10 a 13 y de 15 a 18h</span>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: support -->
            </div>
        </div><!-- #main -->

                <!-- start: footer -->
        <footer class="footer-ar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div><h1 id="logo">Real Trends</h1></div>
                        <div class="social-icons">
                            <a href="https://www.facebook.com/RealTrendshispanos/" target="_blank" class="facebook_light" id="footerFacebookLnk">Facebook<span></span></a>                    
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <nav>
                            <a href="ar/pricing.html" id="footerPricingLnk">Precios</a>
                            <a href="ar.html#support" id="footerSupportLnk">Soporte</a>
                            <a href="ar/termsandconditions.html" id="footerTermsLnk">Términos y condiciones</a>
                            <a href="ar/privacypolicy.html" id="footerPrivacyLnk">Política de privacidad</a>
                        </nav>
                    </div>
                    <div class="col-sm-3">
                        <div class="afip pull-right">
                            <a href="https://qr.afip.gob.ar/?qr=bFBLe7hHLZb5EG7zRTVoFw,," target="_F960AFIPInfo"><img src="https://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" height="56"></a>
                        </div>
                        <div class="logo-cace">
                            <a href="http://www.cace.org.ar/directorio-de-socios/por-nombre/r/" target="_blank"><img src="ar/assets/img/cace.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end: footer -->

        <div class="copyright">
            <div class="text">
                Copyright © 2017 Real Trends es una marca registrada. Todos los derechos reservados. 
            </div>
        </div>

    </div><!-- /#wrapper -->

    <script src="assets/js/wow.min.js"></script>
    
    <script>
        new WOW().init();
    </script>

    <script src="assets/js/retina-1.1.0.js" type="text/javascript" charset="utf-8"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-53583987-1', 'auto');
      ga('send', 'pageview');
      ga('send', 'event', 'view', 'viewed home page AR');
    </script>

    <script>
        window.intercomSettings = {
            app_id: 'qtxrgygu',
            language_override: 'es',
            site_id: 'MLA'
        };
        
        (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qtxrgygu';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()
    </script>

    <script>
        function setCookie(country) {
            var expirationDate = new Date();
            expirationDate.setMonth(expirationDate.getMonth() + 24);
            document.cookie = "country="+country+";expires="+expirationDate+";domain=.real-trends.com;path=/";
            return;
        }

        setCookie('AR');

        var QueryString = function () {
            // This function is anonymous, is executed immediately and 
            // the return value is assigned to QueryString!
            var query_string = {};
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                // If first entry with this name
                if (typeof query_string[pair[0]] === "undefined") {
                    query_string[pair[0]] = decodeURIComponent(pair[1]);
                    // If second entry with this name
                } else if (typeof query_string[pair[0]] === "string") {
                    var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
                    query_string[pair[0]] = arr;
                    // If third or later entry with this name
                } else {
                    query_string[pair[0]].push(decodeURIComponent(pair[1]));
                }
            } 
            return query_string;
        }();

        function setSourceCookie() {
            if (QueryString.utm_source) {
                var expirationDate = new Date();
                expirationDate.setTime(expirationDate.getTime()+(7*24*60*60*1000)); // 7 days
                document.cookie = "reg_source="+QueryString.utm_source+";expires="+expirationDate+";domain=.real-trends.com;path=/";
                return;
            } else if (QueryString.gclid) {
                var expirationDate = new Date();
                expirationDate.setTime(expirationDate.getTime()+(7*24*60*60*1000)); // 7 days
                document.cookie = "reg_source=adwords;expires="+expirationDate+";domain=.real-trends.com;path=/";
                return;
            }
        }

        setSourceCookie();
    </script>

    <script>
        // header events
        $('#headerPricingLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked pricing link in header AR');
        });

        $('#headerSupportLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked support link in header AR');
        });

        $('#headerLoginLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked login link in header AR');
        });

        $('#headerTryLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked try link in header AR');
        });

        //hero events
        $('#heroCertifiedBtn').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked certified button in hero AR');
        });

        // body events
        $('#bodyCertifiedBtn').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked certified button in body AR');
        });

        // footer events
        $('#footerPricingLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked pricing link in footer AR');
        });

        $('#footerSupportLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked support link in footer AR');
        });

        $('#footerTermsLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked terms link in footer AR');
        });

        $('#footerPrivacyLnk').on('click', function(e) {
            ga('send', 'event', 'action', 'clicked privacy link in footer AR');
        });
    </script>

<!-- start Clientes Carousel slider -->
<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav: false,
    autoplay: true,
    autoplayTimeout:500,
    dots: false,
    items:6,
    slideBy:1,
    smartSpeed:1000,
    lazyLoad: true,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:3,
            nav:false
        },
        600:{
            items:5,
            nav:false
        },
        1000:{
            items:6,
            nav:false
        }
    }
})
</script>
<!-- end Clientes Carousel slider -->

</body>
</html>
