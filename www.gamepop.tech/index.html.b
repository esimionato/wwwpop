<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Real Trends | Vendé más en MercadoLibre | Venda mais no MercadoLivre</title>

    <meta name="keywords" content="vender mas, mercadolibre, software, herramientas, seguir competencia, contestar preguntas, subir de posicion, publicar, publicar masivamente, indicadores, metricas, tiempo real, notificaciones, operadores, vender mais, mercadolivre, ferramentas, acompanhar concorrentes, responder perguntas, subir de posição, publicar massivamente, tempo real, notificações" />

    <!-- Search Engine -->
    <meta name="description" content="Seguí de cerca a tus competidores, contestá preguntas más rápido, subí de posición en los listados, publicá masivamente ¡y más!">
    <meta name="image" content="http://www.real-trends.com/assets/images/real-trends-social-media-logo.png">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Real Trends | Vendé más en MercadoLibre | Venda mais no MercadoLivre">
    <meta itemprop="description" content="Seguí de cerca a tus competidores, contestá preguntas más rápido, subí de posición en los listados, publicá masivamente ¡y más!">
    <meta itemprop="image" content="http://www.real-trends.com/assets/images/real-trends-social-media-logo.png">
    
    <link rel="alternate" href="index.html" hreflang="x-default" />
</head>
<body>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-53583987-1', 'auto');
      ga('send', 'pageview');
    </script>
    
    <script type="text/javascript">
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
        }

        function checkCookie() {
            var cookieVal = getCookie('country');
            if (cookieVal == 'AR')
                window.location = "http://www.real-trends.com/ar/";
            else if (cookieVal == 'BR')
                window.location = "http://www.real-trends.com/br/";
            else if (cookieVal == 'MX')
                window.location = "http://www.real-trends.com/mx/";
            else {
                var language = navigator.language || navigator.browserLanguage;

                if (language.indexOf('pt-BR') > -1) {
                    document.location.href = 'http://www.real-trends.com/br/';
                } else if (language.indexOf('es-MX') > -1) {
                    document.location.href = 'http://www.real-trends.com/mx/';
                } else {
                    document.location.href = 'http://www.real-trends.com/ar/';
                }
            }
        }

        checkCookie();
    </script>
</body>
</html>