# Documentacion de GamePop

## Bienvenidos
Bienvenido a la documentacion de GamePop.
Ya seas nuevo en Gamepop o un usuario avanzado, aca podes encontrar informacion util sobre los servicios de Gamepop, desde una descricion de las funcionalidades hasta sus caracteristicas mas avanzadas.


## Caracteristicas Claves

GamePop permite que le pongas foco en contruir tu juego mientras que la plataforma maneja el resto: [cuentas de jugadores](player-accounts.md), [sus perfiles ](player-accounts.md#player-profile),
[su authenticacion dentro del juego](authentication.md), [sus datos, estadisticas](storage-collections.md), [sus acciones, logros dentro del juego](gameplay.md#player-events), y mucho mas.

GamePop incluye una suite de funcionalidades muy amplia: ! Puedes usar tantas como quieras !
<div style="display: flex">
  <div style="flex: 1; margin: 0 1em 0 0">
    <strong>Cuentas de Jugadores</strong>
    <p>Cada <a href="./user-accounts/">jugador</a> registrado obtendra un perfil junto con un inventario, monedas, estadisticas y datos definidos para el juego.</p>
  </div>
  <div style="flex: 1">
    <strong>Autenticacion de Jugadores</strong>
    <p>Cada jugador puede mantener una <a href="./user-accounts/">identidad</a> anomima o autenticarse en el juego.</p>
  </div>
</div>
<div style="display: flex">
  <div style="flex: 1; margin: 0 1em 0 0">
    <strong>Gestion de Juegos y Jugadores</strong>
    <p>Crea juegos, conserva un historial con las configuraciones en cada una de sus versiones.</p>
  </div>
  <div style="flex: 1;">
    <strong>Metricas y Monitoreo</strong>
    <p>Conoce el comportamiento de tus jugadores, equilibra el gameplay, conoce la retencion y el crecimiento de tu juego en tiempo real.</p>
  </div>
</div>

<div style="display: flex">
  <div style="flex: 1; margin: 0 1em 0 0">
    <strong>Herramientas de Monetizacion</strong>
    <p>Defini la economia y el catalogo de tus juegos.</p>
  </div>
  <div style="flex: 1;">
    <strong>Herramientas de Jugabilidad</strong>
    <p>Define las <a href=”/mechanics.md”>mecanicas</a> basicas de gameplay y refinalas con el tiempo, logra una excelente <a href=””>curva de interes</a> para tu juego.</p>
  </div>
</div>


## Organizacion
La documentacion esta organizada en:

 - [Introduction](): presenta que es gamepop.
 - [Conceptos](): son conceptos claves para comprender el funcionamiento de la plataforma.
 - [API Docs](): detalles del uso de los metodos de la API.
 - [Guias y Tutoriales](): tutoriales sobre diversos aspectos de diseno aplicados a juegos completos de ejemplo.
 - [Historial de versiones](): las ultimas actualizaciones de la plataforma.o

## Como empezar ?

Todavia no estas registrado ?

 1. <a href=”http://gampop.tech/register”>Registrate</a> - ! es gratis !
 2. <a href=”/download/sdk.html>Descarga la SDK</a>
 3. Arma tu Juego `m=|^_^|=m`
